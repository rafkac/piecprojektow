# -*- coding: utf-8 -*-
# RafKac
# 2021_07_28
# 2021_07_29
#
# na podstawie kodu z kursu z yt
# rozwinięcie - walidacja wprowadzanych danych
# rozwinięcie - rozbudowa statystyk


expenses = []

def showExpenses(month):
    for expenseAmount, expenseType, expenseMonth in expenses:
        if expenseMonth == month:
            print(f'{expenseAmount} - {expenseType}')


def addExpense(month):
    print()
    global expenses
    expenseAmount = int(input("Podaj kwotę [PLN]: "))
    expenseType = input("Podaj typ wydatków (jedzenie, rozrywka, dom, inny): ")

    expense = (expenseAmount, expenseType, month)
    expenses.append(expense)


def showStats(month):
    totalAmountThisMonth = sum(expenseAmount for expenseAmount, _, expenseMonth in expenses if expenseMonth == month)
    totalAmountAll = sum(expenseAmount for expenseAmount, _, _ in expenses)
    numberOfExpensesThisMounth = sum(1 for _, _, expenseMonth in expenses if expenseMonth == month)
    averageExpenseThisMonth = totalAmountThisMonth/numberOfExpensesThisMounth
    averageExpenseAll = totalAmountAll/len(expenses)

    print()
    print("Statystyki:")
    print("Wszystkie wydatki w tym miesiącu [zł]: ", totalAmountThisMonth)
    print("Wszystkie wydatki [zł]: ", totalAmountAll)
    print("Średni wydatek w tym miesiącu [zł]: ", averageExpenseThisMonth)
    print("Średni wydatek [zł]: ", averageExpenseAll)


while True:
    print()
    month = int(input("Wybierz miesiąc [1-12]: "))

    if month == 0:
        break

    while True:
        print()
        print("0. Powrót do wyboru miesiąca.")
        print("1. Wyświetl wszystkie wydatki.")
        print("2. Dodaj wydatek.")
        print("3. Statystyki.")
        choice = int(input("Wybierz opcję z powyższych: "))

        if choice == 0:
            break

        if choice == 1:
            print("\nWszystkie wydatki.")
            showExpenses(month)

        if choice == 2:
            print("\nDodaj wydatek.")
            addExpense(month)
        
        if choice == 3:
            print("\nStatystyki:")
            showStats(month)