# -*- coding: utf-8 -*-
# RafKac
# 2021_07_28
#
# na podstawie kodu z kursu z yt
#
# program ma za zadanie dostarczyć prosty Quiz
# rozbudowa - można losować pytania z listy, zapisując, które pytania już użytkownik widział
# rozbudowa - kategorie pytań
# rozbudowa - różny poziom trudności i punktacja pytań
# rozbudowa - dodanie opcji << nie wiem>>
# rozbudowa - ranking najlepszych wyników

import json


points = 0


def show_question(question):
    global points
    print()
    print(question["pytanie"])
    print("a:", question["a"])
    print("b:", question["b"])
    print("c:", question["c"])
    print("d:", question["d"])
    print()
    answer = input("Którą odpowiedź wybierasz? ")
    if answer == question["prawidlowa_odpowiedz"]:
        points += 1
        print("Dobrze! Masz {} punktów.".format(points))
    else:
        print("źle! Prawidłowa to: ", question["prawidlowa_odpowiedz"])


with open("quiz.json") as json_file:
    questions = json.load(json_file)

    #print(questions)
    for i in range(0, len(questions)):
        #print(questions[i]["pytanie"])
        show_question(questions[i])

print()
print("Koniec gry.Zdobyta liczba punktów: {}".format(points))