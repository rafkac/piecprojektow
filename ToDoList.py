# -*- coding: utf-8 -*-
# RafKac
# 2021_07_29
#
# na podstawie kodu z kursu z yt

import sqlite3


userChoice = -1

tasks= []
connection = sqlite3.connect("todo.db")


def createTable(connection):
    try:
        cur = connection.cursor()
        cur.execute("""CREATE TABLE tasks(task text)""")
    except:
        print("CreateTable - błąd.")
        pass


def showTasks():
    taskIndex = 0
    for task in tasks:
        print(task + " [" + str(taskIndex) + "]")
        taskIndex += 1


def showTasks2(connection):
    cur = connection.cursor()
    cur.execute("""SELECT rowid, task FROM tasks""")
    result = cur.fetchall()

    for row in result:
        print(str(row[0]) + " - " + row[1])
    print()


def addTask():
    task = input("Wpisz treść zadania: ")
    if task == "0":
        print("Powrót do menu.")
    else:
        tasks.append(task)
        print("Dodano zadanie!")


def addTask2(connection):
    print("Dodajemy zadanie - task2.")
    task = input("Wpisz treść zadania: ")
    if task == "0":
        print("Powrót do menu.")
    else:
        cur = connection.cursor()
        cur.execute("""INSERT INTO tasks(task) VALUES(?)""", (task,))
        connection.commit()
        print("Dodano zadanie. ")


def deleteTask():
    taskIndex = int(input("Podaj indeks zadania do usunięcia: "))
    if taskIndex < 0 or taskIndex > len(tasks) - 1:
        print("Zadanie o tym indeksie nie istnieje.")
        return
    tasks.pop(taskIndex)
    print("Usunięto zadanie!")


def deleteTask2(connection):
    taskIndex = int(input("Podaj indeks zadania do usunięcia: "))

    cur = connection.cursor()
    rowsDeleted = cur.execute("""DELETE FROM tasks WHERE rowid=?""", (taskIndex,)).rowcount
    connection.commit()
    if rowsDeleted == 0:
        print("Takie zadanie nie istnieje.")
    else:
        print("Usunięto zadanie!")


def saveTaskToFile():
    file = open("tasks.txt", "w")
    for task in tasks:
        file.write(task + "\n")
    file.close()


def loadTasksFromFile():
    try:
        file = open("tasks.txt")

        for line in file.readlines():
            tasks.append(line.strip())

        file.close()
    except FileNotFoundError:
        print("Nie ma pliku o podanej nazwie.")
        return
'''
loadTasksFromFile()

while userChoice != 5:
    if userChoice == 1:
        showTasks()
    if userChoice == 2:
        addTask()
    if userChoice == 3:
        deleteTask()
    if userChoice == 4:
        saveTaskToFile()
    print()
    print("1. Pokaż zadania. ")
    print("2. Dodaj zadanie. ")
    print("3. Usuń zadanie. ")
    print("4. Zapisz zmiany do pliku. ")
    print("5. Wyjdź. ")

    userChoice = int(input("Wybierz liczbę [1-4]: "))
    print()
'''
createTable(connection)
while True:
    print("1. Pokaż zadania. ")
    print("2. Dodaj zadanie. ")
    print("3. Usuń zadanie. ")
    print("4. Wyjdź. ")
    userChoice = int(input("Wybierz liczbę [1-4]: "))
    print()
    if userChoice == 1:
#        showTasks()
        showTasks2(connection)
    if userChoice == 2:
#        addTask()
        addTask2(connection)
    if userChoice == 3:
#        deleteTask()
        deleteTask2(connection)
    if userChoice == 4:
        break

connection.close()