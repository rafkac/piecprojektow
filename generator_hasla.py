# -*- coding: utf-8 -*-
# RafKac
# 2021_07_27
#
import sys
import random
import string


password = []
characters_left = -1

def updateCharactersLeft(numberOfCharactes):
    global characters_left
    if numberOfCharactes < 0 or numberOfCharactes > characters_left:
        print("Liczba znaków poza przedziałem [0,", characters_left)
        sys.exit(0)
    else:
        characters_left -= numberOfCharactes
        print("Pozostało znaków: ", characters_left)

password_length = int(input("Jak długie ma być hasło? "))

if password_length < 5:
    print("Hasło jest za krótkie, musi mieć minimum 5 znaków, spróbuj jeszcze raz.")
    sys.exit(0)
else:
    characters_left = password_length

lowercase_letters = int(input("Ile małych liter ma mieć hasło? "))
updateCharactersLeft(lowercase_letters)

uppercase_letters = int(input("Ile dużych liter ma mieć hasło? "))
updateCharactersLeft(uppercase_letters)

special_characters = int(input("Ile znaków specjalnych? "))
updateCharactersLeft(special_characters)

digits = int(input("Ile cyfr ma mieć hasło? "))
updateCharactersLeft(digits)

if characters_left > 0:
    print("Nie wszystkie znaki zostały wykorzystane, hasło zostanie uzupełnione małymi literami.")
    lowercase_letters += characters_left

print()
print("Długość hasła: ", password_length)
print("Małe litery: ", lowercase_letters)
print("Duże litery: ", uppercase_letters)
print("Cyfry: ", digits)

for i in range(password_length):
    if lowercase_letters > 0:
        password.append(random.choice(string.ascii_lowercase))
        lowercase_letters -= 1
    if uppercase_letters > 0:
        password.append(random.choice(string.ascii_uppercase))
        uppercase_letters -= 1
    if special_characters> 0:
        password.append(random.choice(string.punctuation))
        special_characters -= 1
    if digits > 0:
        password.append(random.choice(string.punctuation))
        digits -= 1

random.shuffle(password)
print(password)
print("Wygenerowane hasło: ","".join(password))
print("\n \n A teraz korzystając z biblioteki random:")

password1 = "".join([random.choice(string.ascii_letters + string.punctuation + string.digits) for _ in range(password_length)])
print(password1)








