# -*- coding: utf-8 -*-
# RafKac
# 2021_07_23
# + do rozbudowy - lista słów do losowania, jakie teraz zgadujemy,
# + możemy pamiętać, jakie słowa już zostały wylosowane
# + restart po zakończeniu (czy chce zagrać jeszcze raz?)
# + wybór ilości szans


import sys

no_of_tries = 5
word = 'kamila'
used_letters = []

user_word = []


def findIndexes(word, letter):
    indexes = []
    for index, letter_in_word in enumerate(word):
        if letter == letter_in_word:
            indexes.append(index)
    return indexes


def showStateOfGame():
    print()
    print(user_word)
    print("Pozostało prób: ", no_of_tries)
    print("Uzyte litery: ", used_letters)
    print()


for letter in word:
    user_word.append("_")

while True:
    letter = input("\nPodaj literę: ")
    used_letters.append(letter)

    #print(word.index(letter))
    found_indexes = findIndexes(word, letter)
    if len(found_indexes) == 0:
        print("Nie ma takiej litery.")
        no_of_tries -= 1
#        print("Pozostało prób: ",no_of_tries)
        if no_of_tries == 0:
            print("Koniec gry! ")
            sys.exit(0)
    else:
        for index in found_indexes:
            user_word[index] = letter
#        print(user_word)

        s = "".join(user_word)
#        print(s)
        if s == word:
            print(s)
            print("Zgadłeś! {} złych odpowiedzi.".format((5 - no_of_tries)))
            sys.exit(0)

    showStateOfGame()
#    print("Użyte litery: ", used_letters)